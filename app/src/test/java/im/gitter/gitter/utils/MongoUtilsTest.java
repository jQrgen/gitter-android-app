package im.gitter.gitter.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MongoUtilsTest {

    @Test
    public void idFromTime_isCorrect() {
        assertEquals("56f4072a0000000000000000", MongoUtils.idFromUnixSeconds(1458833194));
    }

    @Test
    public void idFromTimeAtEpoch_isCorrect() {
        assertEquals("000000000000000000000000", MongoUtils.idFromUnixSeconds(0));
    }

    @Test
    public void idFromTimeAfterEpoch_isCorrect() {
        assertEquals("000000010000000000000000", MongoUtils.idFromUnixSeconds(1));
    }

    @Test
    public void unixSecondsFromId_isCorrect() {
        assertEquals(1458833194, MongoUtils.unixSecondsFromId("56f4072a0000000000000000"));
    }

    @Test
    public void unixSecondsFromEpochId_isCorrect() {
        assertEquals(0, MongoUtils.unixSecondsFromId("000000000000000000000000"));
    }
}
